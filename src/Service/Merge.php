<?php


namespace App\Service;


use App\Form\CsvFormType;
use Symfony\Component\HttpFoundation\Request;

class   Merge
{
    public static function read_csv_Fr($csv)
    {
        $filenameFrench = $csv;
        $frenchCsv = fopen($filenameFrench, 'r');
        while ($frenchTab[] = fgetcsv($frenchCsv, 1024, ";")) ;
        fclose($frenchCsv);
        return $frenchTab;
    }
    public static function read_csv_Ger($csv){
        $filenameGerman = $csv;
        $germanCsv = fopen($filenameGerman, 'r');
        while ($germanTab[] = fgetcsv($germanCsv, 1024, ";")) ;
        fclose($germanCsv);
        return $germanTab;
        array_splice($germanTab, 0, 1);

    }

    public static function sequentialMerge($frenchTab, $germanTab){
            $csvModel = 'C:\\Users\\Jerem\\PhpstormProjects\\Symfony\\SPARKLINE\\public\\download\\small-data-empty.csv';
            $emptyFile = fopen($csvModel, 'w');
            foreach ($frenchTab as $value) {
                if ($value) {
                    fputcsv($emptyFile, $value, ',');
                }
            }
            foreach ($germanTab as $key => $value) {
                if ($key != 0) {
                if ($value) {

                    fputcsv($emptyFile, $value, ',');
                }
            }
            }
            fclose($emptyFile);

        }

    public static function interlacedMerge($frenchTab, $germanTab){
            $filenameEmpty = 'C:\\Users\\Jerem\\PhpstormProjects\\Symfony\\SPARKLINE\\public\\download\\small-data-empty.csv';
            $emptyFile = fopen($filenameEmpty, 'w');

            $countTabFR = count($frenchTab) - 1;
            $countTabGER = count($germanTab) - 1;
            $countFR = 1;
            $countGER = 1;
            $count = 0;
            $difTab = 0;
            if ($countTabFR >= $countTabGER) {
                $difTab = $countTabFR - $countTabGER;
                fputcsv($emptyFile, $frenchTab[0], ',');
                while ($count < $countTabGER && $countGER < ($countTabGER)) {
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    $countFR++;
                    $countGER++;
                    $count++;
                }
                while ($difTab !== 0) {
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    $difTab--;
                    $countFR++;
                }
            } else {
                $difTab = $countTabGER - $countTabFR;
                fputcsv($emptyFile, $germanTab[0], ',');
                while ($count < $countTabFR && $countFR < ($countTabFR)) {
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    $countFR++;
                    $countGER++;
                    $count++;
                }
                while ($difTab !== 0) {
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    $difTab--;
                    $countGER++;
                }
            }

    }

}