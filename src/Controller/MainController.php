<?php

namespace App\Controller;

use App\Entity\CsvType;
use App\Service\Merge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CsvFormType;


class MainController extends AbstractController
{


    /**
     * @Route("/main", name="main")
     */
    public function main()
    {
        return $this->render('main/main.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    /**
     * @Route("/download/file", name="file_download")
     */
        public function downloadFile(){

            return $this->render('/main/download.html.twig');


        }



    /**
     * @Route("/merge", name="merge_main")
     */
        public function mainMerge(Request $request)
        {
            $csvForm = $this->CreateForm(CsvFormType::class);
            $csvForm->handleRequest($request);

            if ($csvForm->isSubmitted() && $csvForm->isValid()) {
                $frenchTab = Merge::read_csv_Fr($csvForm->get("csvFile1")->getData());
                $germanTab = Merge::read_csv_Ger($csvForm->get("csvFile2")->getData());

                if ($csvForm->get("isAttending")->getData() == "Sequential") {
                    Merge::sequentialMerge($frenchTab, $germanTab);
                    $this->addFlash('success', 'Sequential Merge Success');

                }
                if ($csvForm->get("isAttending")->getData() == "Interlaced") {
                    Merge::interlacedMerge($frenchTab, $germanTab);
                    $this->addFlash('success', 'Interlaced Merge Success');

                }
                return $this->redirectToRoute("file_download");

            }
            return $this->render('/main/fusion.html.twig', [
                'formulaireCsv' => $csvForm->createView(),
            ]);
        }
}