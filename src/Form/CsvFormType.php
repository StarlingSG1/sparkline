<?php

namespace App\Form;

use App\Entity\CsvType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CsvFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('csvFile1', FileType::class, [
                'label' => 'CSV File 1',
                'mapped' => false,
                'required' => false])
            ->add('csvFile2', FileType::class, [
                'label' => 'CSV File 2',
                'mapped' => false,
                'required' => false,])
            ->add('isAttending', ChoiceType::class, [
                'label' => 'Choose Merge',
                'choices'  => [
                    'Sequential' => "Sequential",
                    'Interlaced' => "Interlaced",

                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }
}
