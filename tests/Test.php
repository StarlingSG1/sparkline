<?php

namespace App\tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service;

class Test extends TestCase
{

    public function testPresenceDonneeTab1()
    {
        $tab = \App\Service\Merge::read_csv_Fr('C:\\Users\\Jerem\\PhpstormProjects\\Symfony\\SPARKLINE\\public\\uploads\\small-french-client.csv');
        $count = count($tab);
        $this->assertGreaterThanOrEqual(0, $count);
    }

    public function testPresenceDonneeTab2()
    {
        $tab = \App\Service\Merge::read_csv_Ger('C:\\Users\\Jerem\\PhpstormProjects\\Symfony\\SPARKLINE\\public\\uploads\\small-german-client.csv');
        $count = count($tab);
        $this->assertGreaterThanOrEqual(0, $count);
    }
}